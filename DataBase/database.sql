/*
database
create database telecms_db;
create user teleusr@localhost identyfied by 'AsgRd';
grant all on telecms_db.* to teleusr@localhost;
 */


create table menu (
  id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  link VARCHAR(500) NOT NULL,
  parent_id INT(10) NULL
) ENGINE=MYISAM DEFAULT CHAR SET utf8;

create TABLE roles (
  id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL
) ENGINE=MYISAM DEFAULT CHAR SET utf8;

CREATE TABLE users (
  id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  login VARCHAR(200) NOT NULL,
  passw VARCHAR(32) NOT NULL,
  role_id INT(10) NOT NULL,
  date_reg TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MYISAM DEFAULT CHAR SET utf8;

CREATE TABLE posts (
  id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(500) NULL,
  body TEXT NULL,
  date_add TIMESTAMP,
  is_draft TINYINT(1),
  is_static_page TINYINT(1),
  user_id INT(10)
) ENGINE=MYISAM DEFAULT CHAR SET utf8;

CREATE TABLE tags(
  id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(200) NOT NULL
) ENGINE=MYISAM DEFAULT CHAR SET utf8;

CREATE TABLE posts_tags(
  tag_id INT(10) NOT NULL,
  post_id INT(10) NOT NULL
) ENGINE=MYISAM DEFAULT CHAR SET utf8;

ALTER TABLE menu ADD CONSTRAINT fk_menu FOREIGN KEY (id) REFERENCES menu(parent_id);
ALTER TABLE roles ADD CONSTRAINT fk_roles_users FOREIGN KEY (id) REFERENCES users(role_id);
ALTER TABLE users ADD CONSTRAINT fk_users_posts FOREIGN KEY (id) REFERENCES posts(user_id);
ALTER TABLE posts ADD CONSTRAINT fk_posts_tags_s FOREIGN KEY (id) REFERENCES posts_tags(post_id);
ALTER TABLE tags ADD CONSTRAINT fk_posts_tags_n FOREIGN KEY (id) REFERENCES posts_tags(tag_id);